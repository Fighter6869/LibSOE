import time


class SOEClient(object):
	def __init__(manager, client):
		self.sessions_srarted = False
		self.session_id = 0
		self.crc_length = 0
		self.buffer_size = 0
		self.server= manager.server
		self.manager = manager
		self.client = client 
		self.data_channel = SOEDataChannel(self)
		self.Encrypted = False

	def set_client_id(self,id):
		self.set_client_id= id
		self.Interact()

	def StartSession(self,crc_length,session_id,udpBuffferSize):
		self.CRC_Seed=1884358976
		self.CRC_Length = crc_length
		self.Session_ID= session_id
		self.BufferSize= udpBuffferSize
		self.SessionStarted = True 
		self.Compressable =True
		self.Encrypted = False
		self.Encryptable =False

	def Toggle_Encryption(self):
		self.Encrypted= not self.Encrypted
		
	def Encrypt(self,data):
		return self.Server.Protocol.Compress(self,data)

	def	Compress(self):
		return Server.Protocol.Compress(self,data)

	def GetAppendeedCRC32(self,packet):
		self.finalCRCBytes = new [CRC_Length]
		self.crc=GetCRC32Checksum(self,packet)
		self.place=0
		self.crcBytes = BitConverter.GetBytes(crc).Reverse().ToArray()
		final_crc_bytes = bytearray()
		start = 4 - self.crc_length
		crc_slice = crc_bytes[start:]
		for byte in crc_slice:
			final_crc_bytes.append(byte)

	def GetCRCChecksum(self,packet):
		return self.Server.Protocol.GetCRCChecksum(CRCSeed,packet)

	def Disconnect(self,reason,client = False):
		return self.Manger.DisconnectClient(self,reason,client)

	def SendPacket(self,packet):
		self.Server.SendPacket(self,packet)
		self.Interact()	

	def SendMessage(self,message):
		self.data_channel.Send(message)
		self.Interact()

	def ReceiveMessage(self,rawMessage):
		self.Server.ReceiveMessage(self, rawMessage)
		self.Interact()

	def GetNextSequenceNumber(self):
		return self.data_channel.GetNextSequenceNumber()
		
	def Interact(self):
		self.Last_Interaction = time.time()